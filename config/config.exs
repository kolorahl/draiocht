use Mix.Config

config :goth, json: Path.expand("google-cloud.json", __DIR__) |> File.read!

config :draiocht, port: 9292

import_config "#{Mix.env}.exs"
