defmodule Draiocht.Routers.Json do
  import Plug.Conn

  defmacro __using__(_) do
    quote do
      # We want to be a Plug Router.
      use Plug.Router

      # Import JSON-related router functions for convenience.
      import Draiocht.Routers.Json
    end
  end

  # Adds the appropriate response content type for JSON payloads.
  def json_resp_header(conn) do
    put_resp_content_type(conn, "application/json")
  end

  # Sets the appropriate JSON-specific response headers, encodes the payload
  # into a JSON string, then sends it to the client.
  def json_resp(conn, payload, status \\ 200) do
    conn |>
      json_resp_header |>
      send_resp(status, Poison.encode!(payload))
  end
end
