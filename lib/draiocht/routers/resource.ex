defmodule Draiocht.Routers.Resource do
  alias Plug.Conn
  alias Draiocht.Routers.Json

  defmacro __using__(_) do
    quote do
      use Draiocht.Routers.Json
      import Draiocht.Routers.Resource
    end
  end

  def list(conn, resource, _opts) do
    conn = Conn.fetch_query_params(conn)
    limit = query_limit(conn)
    order = query_order(conn)

    res = Draiocht.Datastore.list(resource, limit: limit, order: order)
    Json.json_resp(conn, %{data: res})
  end

  def get(conn, resource, id, _opts) do
    case Draiocht.Datastore.get({resource, id}) do
      {:ok, res}  -> Json.json_resp(conn, res)
      {:error, _} -> Json.json_resp(conn, %{error: "resource not found"}, 404)
    end
  end

  defp query_p(conn, key, default) do
    Map.get(conn.query_params, key, default)
  end

  defp query_limit(conn) do
    query_p(conn, "limit", 10)
  end

  defp query_order(conn) do
    order = query_p(conn, "order", %{"created_at" => "desc"})
    Enum.into(order, [], fn({k, v}) ->
      {String.to_atom(k), v}
    end)
  end
end
