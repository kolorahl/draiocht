defmodule Draiocht.Layout do
  def get(name) do
    Path.expand("layouts/#{name}.mustache", Draiocht.priv_dir)
  end

  def prepare(name) when is_binary(name) or is_atom(name) do
    template = get(name) |> :bbmustache.parse_file
    prepare(template)
  end
  def prepare(template), do: template

  def render(name_or_template), do: render(name_or_template,
                                           default_data,
                                           default_opts)

  def render(name_or_template, data), do: render(name_or_template,
                                                 data,
                                                 default_opts)

  def render(name, data, opts) when is_binary(name) or is_atom(name) do
    prepare(name) |> render(data, opts)
  end
  def render(template, data, opts) do
    :bbmustache.compile(template, data, opts)
  end

  defp default_data, do: %{}
  defp default_opts, do: [key_type: :atom]
end
