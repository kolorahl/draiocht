defmodule Draiocht.Router do
  use Plug.Router

  plug Plug.Logger
  plug Plug.Static, at: "/assets", from: Draiocht.priv_dir

  plug :match
  plug :dispatch

  get "/health" do
    send_resp(conn, 204, "")
  end

  get "/" do
    send_resp(conn, 200, render_layout("index"))
  end

  get "/posts" do
    send_resp(conn, 200, render_layout("post"))
  end

  get "/pages" do
    send_resp(conn, 200, render_layout("page"))
  end

  forward "/api/posts", to: Draiocht.PostsRouter
  forward "/api/pages", to: Draiocht.PagesRouter

  match _ do
    send_resp(conn, 404, "not found")
  end

  def nav_pages do
    [
      %{title: "About", link: "/#pages/about"},
      %{title: "Open Source", link: "/#pages/oss"}
    ]
  end

  def render_layout(page, data \\ %{}) do
    Draiocht.Layout.render(page, Map.merge(%{pages: nav_pages}, data))
  end
end
