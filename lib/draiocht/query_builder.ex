defmodule Draiocht.QueryBuilder do
  def build(kind, opts \\ []) do
    []
    |> add_select(kind, opts)
    |> add_filter(opts)
    |> add_order(opts)
    |> add_limit(opts)
    |> join_query_components
  end

  defp add_select(components, kind, opts) do
    attributes = case Keyword.fetch(opts, :select) do
                   {:ok, attrs} -> "#{Enum.join(attrs, ", ")}"
                   :error -> "*"
                 end
    ["select #{attributes} from `#{kind}`" | components]
  end

  defp add_filter(components, opts) do
    case Keyword.fetch(opts, :where) do
      {:ok, filters} -> [ "where #{build_filters(filters)}" | components ]
      :error -> components
    end
  end

  defp build_filters(filters) do
    Enum.map(filters, fn({key, value}) ->
      "#{key} = #{value}"
    end) |> Enum.join(" and ")
  end

  defp add_order(components, opts) do
    case Keyword.fetch(opts, :order) do
      {:ok, ordering} -> [ "order by #{build_order(ordering)}" | components ]
      :error -> components
    end
  end

  defp build_order(ordering) do
    Enum.map(ordering, fn
      ({key, :asc}) -> "#{key} asc"
      ({key, "asc"}) -> "#{key} asc"
      ({key, :desc}) -> "#{key} desc"
      ({key, "desc"}) -> "#{key} desc"
      _ -> ""
    end) |> Enum.join(", ")
  end

  defp add_limit(components, opts) do
    case Keyword.fetch(opts, :limit) do
      {:ok, limit} -> [ "limit #{limit}" | components ]
      :error -> components
    end
  end

  defp join_query_components(components) do
    components |> Enum.reverse |> Enum.join(" ")
  end
end
