defmodule Draiocht.StaticAssetPlug do
  use Plug.Builder

  plug Plug.Static, at: "/assets", from: :draiocht
end
