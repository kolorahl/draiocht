defmodule Draiocht.PostsRouter do
  use Draiocht.Routers.Resource

  @resource "Post"

  plug :match
  plug :dispatch

  get "/" do
    list(conn, @resource, [])
  end

  get "/:id" do
    get(conn, @resource, id, [])
  end

  def post_to_id(post) do
    title = post["title"]
    |> String.slice(0, 30)
    |> String.downcase
    |> String.replace(" ", "-")
    |> URI.encode

    "#{title}-#{post["created_at"]}"
  end
end
