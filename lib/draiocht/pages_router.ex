defmodule Draiocht.PagesRouter do
  use Draiocht.Routers.Resource

  @resource "Page"

  plug :match
  plug :dispatch

  get "/" do
    list(conn, @resource, [])
  end

  get "/:id" do
    get(conn, @resource, id, [])
  end
end
