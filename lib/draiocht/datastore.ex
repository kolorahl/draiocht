defmodule Draiocht.Datastore do
  alias Diplomat.Entity
  alias Diplomat.Key
  alias Diplomat.Query
  alias Diplomat.Value
  alias Draiocht.QueryBuilder

  def key_literal({kind, id}) do
    "key(`#{kind}`, '#{id}')"
  end

  def query(q, v \\ []) do
    Query.new(q, v) |> Query.execute |> process_result
  end

  def list(kind, opts \\ []) do
    opts = Keyword.put_new(opts, :limit, 10)
    values = Keyword.get(opts, :values, [])
    QueryBuilder.build(kind, opts) |> query(values)
  end

  def get({kind, id}) do
    QueryBuilder.build(kind, where: [__key__: key_literal({kind, id})]) |> get_q
  end
  def get({kind, id, field}) do
    QueryBuilder.build(kind, where: [{field, id}]) |> get_q
  end

  defp get_q(q) do
    case query(q) do
      [res] -> {:ok, res}
      err   -> {:error, err}
    end
  end

  def insert(key, data) do
    entity(key, data) |> Entity.insert
  end

  def upsert(key, data) do
    entity(key, data) |> Entity.upsert
  end

  def update(key, data) do
    [ {:upsert, entity(key, data)} ]
    |> Entity.commit_request
    |> Diplomat.Client.commit
    |> handle_update
  end

  def delete({kind, id}) do
    [ {:delete, Key.new(kind, id)} ]
    |> Entity.commit_request
    |> Diplomat.Client.commit
    |> handle_delete
  end

  defp entity({kind, id}, data) do
    Entity.new(data, kind, id)
  end

  def process_result({:error, error}) do
    IO.puts "Received error from datastore:"
    IO.puts error
    {:error, error}
  end
  def process_result(entities) do
    entity_to_map(entities)
  end

  defp entity_to_map(entities) when is_list(entities) do
    Enum.map(entities, &entity_to_map/1)
  end
  defp entity_to_map(entity) do
    entity.properties
    |> Enum.map(&map_value/1)
    |> add_entity_key(entity)
    |> Enum.into(%{})
  end

  defp map_value({k, %Value{value: %Key{kind: kind, name: id}}=v}) do
    case get({kind, id}) do
      {:ok, value} -> {k, value}
      {:error, _}  -> {k, v}
    end
  end
  defp map_value({k, %Value{value: v}}) do
    {k, v}
  end

  defp add_entity_key(list, %Entity{key: %Key{name: key}}) do
    [{"key", key} | list]
  end

  defp handle_update({:ok, resp}), do: resp
  defp handle_update(error), do: error

  defp handle_delete({:ok, _}), do: :ok
  defp handle_delete(error), do: error
end
