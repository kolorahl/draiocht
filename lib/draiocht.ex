defmodule Draiocht do
  use Application

  # Get an environment variable or return a default if missing.
  def env(key, default \\ nil) do
    Application.get_env(:draiocht, key, default)
  end

  # Get an environment variable or raise an error if missing.
  def env!(key) do
    Application.fetch_env!(:draiocht, key)
  end

  # Get the path to the project's priv directory.
  def priv_dir do
    Path.expand("../priv", __DIR__)
  end

  def start(_type, _args) do
    children = [
      Plug.Adapters.Cowboy.child_spec(:http,
                                      Draiocht.Router,
                                      [],
                                      port: env(:port, 9292))
    ]

    opts = [
      strategy: :one_for_one,
    ]

    Supervisor.start_link(children, opts)
  end
end
