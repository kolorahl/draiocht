# draiocht

draíocht (DREE-oct) [noun]

1. druidic arts
2. magic, enchantment

## What is it?

Originally I was thinking static-site generator. But since then I've decided
that I wanted to get a bit more fancy. This is a blogging tool that serves blog
content using a web-based API.

I wanted to use [Elixir](http://elixir-lang.org/) for the server language
because I loved Erlang when I got to use it and Elixir really does feel like
it's become a solid Erlang 1.5 (I'm not quite ready to call it Erlang 2.0).

I wanted to use [Elm](http://elm-lang.org/) for the client language because I
really like the idea of a client language that is functional and operates using
immutable data and a virtual DOM. And recent changes have made Elm even more
friendly to developers without losing any of the original charm.

## Architecture

### It's not Phoenix

I know some people will look here and say "Why not just use Phoenix Framework to
do most of this?" I think Phoenix Framework is perfectly acceptable for most web
applications. It might even be a great choice for API services. But I was
creating web apps and APIs with Erlang and Cowboy a long time ago, so I'm very
comfortable with getting a little lower level than Phoenix Framework. I also
don't like Rails much and the fact they are always compared makes me want to use
it less.

However, I don't feel the need to start from the very bottom and build the
foundation for everything. For most of the boilerplate I've opted to use
Elixir's Plug library.

### Plug Away

[Plug](https://github.com/elixir-lang/plug) gives Elixir projects a composable
spec for quickly and simply building web applications. The project also comes
with a lot of great starting Plugs that can do most of what you would likely
want from a web app. For example, there's a Cowboy Plug that will start Cowboy,
accept incoming requests, and route them in a simple manner. And `Plug.Router`
allows develoeprs to easily control which endpoints get directed to which
handler functions. There's even `Plug.Static` for very easily setting up a path
for serving static files more efficiently. It's all quite wonderful.

Plug for Elixir is basically your building blocks for creating web-based
applications.

### Definitely not JavaScript

No Node, no Angular, no CoffeeScript, none of that. I hate JavaScript
immensely. It is in dire need of an update - more of a full-on replacement - but
instead we keep using an obviously outdated and inadequate tool to get the job
done because everyone knows it and JavaScript happens to be The One Ring in
terms of browser languages.

I like React and Elm best. React is great because it also operates using a
virtual DOM and also allows me to write pseudo-HTML with a dash of JavaScript
where necessary. But even though I like React, it doesn't hold a candle to Elm
for me. Elm compiles to a fairly large JavaScript payload, but it's native
JavaScript that can run any place you need it to. And it's fast. And the syntax
is friendly. And the compiler is awesome (even if the error messages are
sometimes misleading).

Elm and React both allow me to write "HTML" in their code, thanks to their
virtual DOM implementations, so it's a very programatic way for developers to
create client code that updates state and then renders the new state from
scratch but in a very short amount of time. Since the goal was to create a
blog API and not just a blog website, it made sense to choose one of these since
I'd be pulling in data from the API and then rendering state changes from
new/updated data.

## Building

1. `docker build -t draiocht .`

That's it. You can test it with `docker run --rm -ti -p 9292:9292 draiocht`

## Kubernetes Setup

1. Create deployment: `kubectl create -f blog-deployment.yaml`.
2. Create ingress to direct HTTP traffic to backend draiocht service:
   `kubectl create -f blog-ingress.yaml`.

## Deploying

Assumes GKE (Google Container Engine) deployment.

1. `gcloud docker -- push <tag>:<version>`
2. `kubectl edit deployment <name>`
3. Modify `spec.template.spec.containers.image` nested key to match the
   `<tag>:<version>` pushed in step 1.
4. Monitor rollout with `kubectl rollout status <name>` and
   `kubectl rollout history <name>`.
