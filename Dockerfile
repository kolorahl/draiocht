FROM msaraiva/elixir-dev

EXPOSE 9292

RUN mkdir -p /home/draiocht
RUN mkdir -p /home/draiocht/priv/layouts
RUN mkdir -p /home/draiocht/rel

WORKDIR /home/draiocht

COPY mix.exs .
COPY mix.lock .
COPY config config
COPY frontend frontend
COPY lib lib
COPY priv priv
COPY rel/config.exs rel/config.exs

ENV MIX_ENV prod

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix compile
RUN mix release


CMD ["/home/draiocht/rel/draiocht/bin/draiocht", "foreground"]
