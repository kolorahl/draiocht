defmodule Draiocht.Mixfile do
  use Mix.Project

  def project do
    [app: :draiocht,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [
      applications: [:logger, :cowboy, :plug, :goth],
      included_applications: [:bbmustache, :diplomat, :poison],
      mod: {Draiocht, []},
    ]
  end

  defp deps do
    [
      {:cowboy, "~> 1.0"},
      {:plug, "~> 1.2"},
      {:bbmustache, "~> 1.1"},
      {:poison, "~> 2.2"},
      {:diplomat, "~> 0.1"},
      {:distillery, "~> 0.9"},
    ]
  end
end
