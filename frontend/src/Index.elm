port module Index exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Http
import Markdown
import Navigation
import Regex exposing (regex)
import String
import Task

import Page
import Post
import View.Index
import View.Post
import View.Page

import Debug exposing (log)

type Msg = NoOp
         | LoadIndex View.Index.Model
         | LoadPost View.Post.Model
         | LoadPage View.Page.Model
         | FetchError Http.Error

{-| Top-level view type. -}
type View = Index View.Index.Model
          | Post View.Post.Model
          | Page View.Page.Model

{-| Simple data model based on the concept of views and view models (VMs). -}
type alias Model =
  { view   : View
  , error  : Maybe String
  , url    : String
  , loaded : Bool
  }

{-| Actions that can trigger for any page view. -}
type Action = Load
            | Render


{- General Functions -}

main =
  Navigation.program (Navigation.makeParser urlParser)
       {
         init = init
       , urlUpdate = urlUpdate
       , view = view
       , update = update
       , subscriptions = \_ -> Sub.none
       }

baseUrlParser location =
  location.protocol ++ "//" ++ location.host

viewParser hash =
  let reg = Regex.split (Regex.AtMost 1) (regex "/")
  in  case (String.uncons hash) of
        Just ('#', rest) -> reg rest
        _ -> ["index", ""]

viewBuilder data =
  case data of
    "posts" :: id :: [] -> Post { id = id, post = Nothing }
    "pages" :: id :: [] -> Page { id = id, page = Nothing }
    _ -> Index { start = 0, limit = 5, posts = [] }

urlParser location =
  let url  = baseUrlParser location
      data = viewParser location.hash
      view = viewBuilder data
  in  (url, view)

urlUpdate (url, view) model =
  init (url, view)

init (url, view) =
  let model = { url = url, view = view, error = Nothing, loaded = False }
  in  model ! [ loadView model ]

loadView model =
  case model.view of
    Index _ -> loadIndex model
    Post _  -> loadPost model
    Page _  -> loadPage model

httpErrorString error =
  case error of
    Http.UnexpectedPayload s -> s
    Http.BadResponse _ s -> s
    _ -> "unexpected error"

update msg model =
  case msg of
    NoOp -> model ! []
    LoadIndex index  -> { model | view = Index index, error = Nothing, loaded = True } ! []
    LoadPost post    -> { model | view = Post post, error = Nothing, loaded = True } ! []
    LoadPage page    -> { model | view = Page page, error = Nothing, loaded = True } ! []
    FetchError error -> { model | error = Just (httpErrorString error), loaded = True } ! []

renderView model =
  case model.view of
    Index _ -> renderIndex model
    Post _  -> renderPost model
    Page _  -> renderPage model

view model =
  case model.loaded of
    True -> case model.error of
              Just msg -> renderError msg
              Nothing -> renderView model
    False -> div [ class "loading" ] [ text "Thanks for your patience while we load your content!" ]

renderError msg =
  div
    [ class "error" ]
    [ text "Something went wrong!"
    , text msg
    ]

renderIncorrectModel =
  div
    [ class "error" ]
    [ text "The internal state has been screwd up somehow. Sorry about that, my bad. Please refresh the page and try again." ]


loadIndex model =
  let convert posts = LoadIndex { start = 0, limit = 5, posts = posts }
  in  Task.perform FetchError convert (Post.fetchList model.url)

renderIndex model =
  case model.view of
    Index data -> View.Index.render model data
    _ -> renderIncorrectModel

loadPost model =
  case model.view of
    Post data ->
      let convert post = LoadPost { data | post = Just post }
      in  Task.perform FetchError convert (Post.fetch model.url data.id)
    _ -> Cmd.none

renderPost model =
  case model.view of
    Post data -> View.Post.render model data
    _ -> renderIncorrectModel

loadPage model =
  case model.view of
    Page data ->
      let convert page = LoadPage { data | page = Just page }
      in  Task.perform FetchError convert (Page.fetch model.url data.id)
    _ -> Cmd.none

renderPage model =
  case model.view of
    Page data -> View.Page.render model data
    _ -> renderIncorrectModel
