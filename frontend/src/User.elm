module User exposing (..)

import Json.Decode as Decode exposing ((:=))

type alias User =
  { id : String
  , name : String
  }

decode =
  Decode.object2 User
          ("key" := Decode.string)
          ("name" := Decode.string)
