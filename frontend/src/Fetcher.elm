module Fetcher exposing ( url
                        , fetch
                        )
{-| Module to assist with fetching data from a REST API.

# API Functions
@docs url, fetch
-}

import Json.Decode as Decode exposing ((:=))
import Http exposing (Request)
import String
import Task

{-| Build an API URL from a set of URL components. -}
url : String -> List String -> String
url base parts =
  let all  = base :: parts
  in  String.join "/" all

{-| Fetch data from an API. -}
fetch : Decode.Decoder a -> String -> Task.Task Http.Error a
fetch decoder url =
  Http.get decoder url
