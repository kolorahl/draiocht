port module View.Index exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Markdown
import String
import Task

import Fetcher
import Post
import View.Post

type alias Model =
  { start  : Int
  , limit  : Int
  , posts  : List Post.Post
  }

render global model =
  let html = case model.posts of
               [] -> [ div [] [ text "No posts! Someone should create one..." ] ]
               posts  -> List.map (renderPreview global) posts
  in  div [ class "posts" ] html

renderPreview global post =
  let link = postLink global.url post
      words = List.take 1 (String.split "\n\n" post.body)
      cont  = String.join "" [" [Continue Reading](", link, ")"]
      preview = String.join " " (words ++ [cont])
  in  div
        [ id post.id, class "post" ]
        [ h2 [ class "title" ]   [ linkToPost global.url post ]
        , h5 [ class "author" ]  [ View.Post.authorText post ]
        , Markdown.toHtml [ class "preview" ] preview
        ]

linkToPost url post =
  let url = postLink url post
  in  a [ href url ] [ text post.title ]

postLink url post =
  Fetcher.url url ["#posts", post.id]
