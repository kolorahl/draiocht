port module View.Post exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Markdown
import String
import Task

import Post

type alias Model =
  { id   : String
  , post : Maybe Post.Post
  }

render global model =
  case model.post of
    Nothing -> div [ class "error" ] [ text "Post did not load properly." ]
    Just post -> div
                   [ id post.id, class "post" ]
                   [ h1 [ class "title" ]  [ text post.title ]
                   , h5 [ class "author" ] [ authorText post ]
                   , Markdown.toHtml [ class "body" ] post.body
                   ]

authorText post =
  text ("by " ++ post.author.name)
