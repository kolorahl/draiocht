port module View.Page exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Markdown
import String
import Task

import Page

type alias Model =
  { id   : String
  , page : Maybe Page.Page
  }

render global model =
  case model.page of
    Nothing -> div [ class "error" ] [ text "Page did not load properly." ]
    Just page -> Markdown.toHtml [ class "page" ] page.content
