module Post exposing ( Post
                     , decode
                     , decodeList
                     , url
                     , fetch
                     , fetchList
                     )
{-| Contains types and functions used to represent, decode, encode, and retrieve
posts from the API.

# Definitions
@docs Post

# Decoding/Encoding
@docs decode, decodeList

# API Helpers
@docs url, fetch, fetchList
-}

import Json.Decode as Decode exposing ((:=))
import Http
import String
import Task

import Fetcher
import User

{-| Represents a Post object. -}
type alias Post =
  { id : String
  , title : String
  , author : User.User
  , body : String
  , created_at : Int
  , updated_at : Int
  }

{-| Decode a single Post object from some JSON. -}
decode : Decode.Decoder Post
decode =
  Decode.object6 Post
          ("key" := Decode.string)
          ("title" := Decode.string)
          ("author" := User.decode)
          ("body" := Decode.string)
          ("created_at" := Decode.int)
          ("updated_at" := Decode.int)

{-| Decode a list of Post objects from the 'data' field in some JSON. -}
decodeList : Decode.Decoder (List Post)
decodeList =
  Decode.at ["data"] (Decode.list decode)

{-| Generate an API posts endpoint URL. -}
url : String -> List String -> String
url base extra =
  Fetcher.url base (["api", "posts"] ++ extra)

{-| Fetch a single post from the API. -}
fetch : String -> String -> Task.Task Http.Error Post
fetch base id =
  fetch' decode base [id]

{-| Fetch a collection of posts from the API. -}
fetchList : String -> Task.Task Http.Error (List Post)
fetchList base =
  fetch' decodeList base []

{-| Perform the actual fetch. -}
fetch' decoder base extra =
  Fetcher.fetch decoder (url base extra)
