module Page exposing ( Page
                     , decode
                     , decodeList
                     , url
                     , fetch
                     , fetchList
                     )
{-| Contains types and functions used to represent, decode, encode, and retrieve
pages from the API.

# Definitions
@docs Page

# Decoding/Encoding
@docs decode, decodeList

# API Helpers
@docs url, fetch, fetchList
-}

import Json.Decode as Decode exposing ((:=))
import Http
import String
import Task

import Fetcher
import User

{-| Represents a Page object. -}
type alias Page =
  { id : String
  , title : String
  , content : String
  , created_at : Int
  , updated_at : Int
  }

{-| Decode a single Page object from some JSON. -}
decode : Decode.Decoder Page
decode =
  Decode.object5 Page
          ("key" := Decode.string)
          ("title" := Decode.string)
          ("content" := Decode.string)
          ("created_at" := Decode.int)
          ("updated_at" := Decode.int)

{-| Decode a list of Page objects from the 'data' field in some JSON. -}
decodeList : Decode.Decoder (List Page)
decodeList =
  Decode.at ["data"] (Decode.list decode)

{-| Generate an API pages endpoint URL. -}
url : String -> List String -> String
url base extra =
  Fetcher.url base (["api", "pages"] ++ extra)

{-| Fetch a single page from the API. -}
fetch : String -> String -> Task.Task Http.Error Page
fetch base id =
  fetch' decode base [id]

{-| Fetch a collection of pages from the API. -}
fetchList : String -> Task.Task Http.Error (List Page)
fetchList base =
  fetch' decodeList base []

{-| Perform the actual fetch. -}
fetch' decoder base extra =
  Fetcher.fetch decoder (url base extra)
